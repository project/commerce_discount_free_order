<?php

/**
 * @file
 * Rules integration for the Commerce Discount Free Order module.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_discount_free_order_rules_action_info() {
  return array(
    'commerce_discount_free_order' => array(
      'label' => t('Free order'),
      'group' => t('Commerce Discount'),
      'parameter' => array(
        'entity' => array(
          'label' => t('Entity'),
          'type' => 'entity',
          'wrapped' => FALSE,
        ),
        'commerce_discount' => array(
          'label' => t('Commerce Discount'),
          'type' => 'token',
          'options list' => 'commerce_discount_entity_list',
        ),
      ),
      'base' => 'commerce_discount_free_order',
    )
  );
}

/**
 * Attach a discount line item on order to make it free.
 *
 * @param object $order
 *   A fully loaded order object.
 * @param string $discount_name
 *   The discount name.
 *
 * @return void
 */
function commerce_discount_free_order($order, $discount_name) {
  // Wrap the order.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  // Require the rules file from commerce_discount module in order to get access
  // to its own functions.
  module_load_include('rules.inc', 'commerce_discount');

  $discount_amount = array(
    'amount' => $order_wrapper->commerce_order_total->amount->value() * -1,
    'currency_code' => $order_wrapper->commerce_order_total->currency_code->value(),
  );

  // Modify the existing discount line item or add a new one if that fails.
  if (!commerce_discount_set_existing_line_item_price($order_wrapper, $discount_name, $discount_amount)) {
    commerce_discount_add_line_item($order_wrapper, $discount_name, $discount_amount);
  }

  // Update the total order price, for the next rules condition (if any).
  commerce_order_calculate_total($order_wrapper->value());
}
